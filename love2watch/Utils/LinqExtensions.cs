using System;
using System.Linq.Expressions;

namespace LinqKit
{
    /// <summary>
    ///     Add extra methods to LinqKit classes
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        ///     Convert `ExpressionStarter` created by `PredicateBuilder.New` into plain LINQ Expression
        ///     https://stackoverflow.com/a/46716258/978434
        /// </summary>
        /// <remarks>
        ///     In some cases you can not use the result of PredicateBuilder as predicate. One example is
        ///     using predicate for related models.
        ///     And so you need to convert it to expression outside the query.
        /// </remarks>
        /// <example>
        ///     <code>
        ///     // So you have a predicate
        ///     var castCondition = PredicateBuilder.New&lt;CastInfo>(true);
        ///     castCondition = castCondition.Add(...);
        ///     // And you can not use it like this
        ///     context.Name.AsExpandable().Where(n => n.CastInfo.Any(castCondition));
        ///     // And even you can not compile it inline
        ///     context.Name.AsExpandable().Where(n => n.CastInfo.Any(castCondition.Build().Compile()));
        ///     // You need an extra variable
        ///     var castPredicate = castCondition.Build();
        ///     context.Name.AsExpandable().Where(n => n.CastInfo.Any(castPredicate.Compile()));
        ///     </code>
        /// </example>
        /// <param name="expr">Finished condition</param>
        /// <returns>Predicate converted to plain expression</returns>
        public static Expression<Func<T,bool>> Build<T>(this ExpressionStarter<T> expr) =>
            (Expression<Func<T,bool>>)expr;
    }
}