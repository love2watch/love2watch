﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpgsqlTypes;
using Npgsql.PostgresTypes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace love2watch.Models
{

    /// <summary>
    ///     VIEW that is used to keep FTS search for <see cref="Name"/> table.
    /// </summary>
    [Table("name_and_aka")] 
    public class NameSearch
    {
        /**
         * WARNING: This is not the table, but the view, used for text search
         * See https://trello.com/c/o9Bwpos6 on creation manual
         * DO NOT create it with migration!
         */
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("all_names_vector")]
        public NpgsqlTsVector AllNamesVector { get; set; }
        [Column("all_roles")]
        public int[] AllRoles { get; set; }

        [Column("roles_count")]
        public int RolesCount { get; set; }

        /// <summary>
        ///     ID of view is also ID of person (<see cref="Name"/>)
        /// </summary>
        [ForeignKey("Id")]
        public Name RelatedPerson { get; set; }
    }

    /// <summary>
    ///     VIEW that is used to keep FTS indexes for <see cref="Title"/> table.
    /// </summary>
    [Table("title_and_aka")]
    public class MovieSearch 
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("all_names_vector")]
        public NpgsqlTsVector AllNamesVector { get; set; }

        /// <summary>
        ///     ID in the view is also ID of the movie (<see cref="Title"/>)
        /// </summary>
        [ForeignKey("Id")]
        public Title RelatedTitle { get; set; }
    }
}
/**
SQL for movie search
SELECT t1.id,
    t1.title,
    (setweight(to_tsvector(t1.title), 'A') || setweight(to_tsvector(COALESCE(string_agg(t2.title, ' '), '')), 'B')) AS all_names_vector

   FROM (title t1
     LEFT JOIN aka_title t2 ON t2.movie_id = t1.id
)
  WHERE (t1.title ~~* 'P%'::text)
  GROUP BY t1.id
 */