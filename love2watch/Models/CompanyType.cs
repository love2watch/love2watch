﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CompanyType
    {
        public CompanyType()
        {
            MovieCompanies = new HashSet<MovieCompanies>();
        }

        public int Id { get; set; }
        public string Kind { get; set; }

        public ICollection<MovieCompanies> MovieCompanies { get; set; }
    }
}
