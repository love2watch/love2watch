﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CompanyName
    {
        public CompanyName()
        {
            MovieCompanies = new HashSet<MovieCompanies>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public int? ImdbId { get; set; }
        public string NamePcodeNf { get; set; }
        public string NamePcodeSf { get; set; }
        public string Md5sum { get; set; }

        public ICollection<MovieCompanies> MovieCompanies { get; set; }
    }
}
