﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class Keyword
    {
        public Keyword()
        {
            MovieKeyword = new HashSet<MovieKeyword>();
        }

        public int Id { get; set; }
        public string Keyword1 { get; set; }
        public string PhoneticCode { get; set; }

        public ICollection<MovieKeyword> MovieKeyword { get; set; }
    }
}
