﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class LinkType
    {
        public int Id { get; set; }
        public string Link { get; set; }
    }
}
