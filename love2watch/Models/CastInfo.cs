﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CastInfo
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int MovieId { get; set; }
        public int? PersonRoleId { get; set; }
        public string Note { get; set; }
        public int? NrOrder { get; set; }
        public int RoleId { get; set; }

        public Title Movie { get; set; }
        public Name Person { get; set; }
        public CharName PersonRole { get; set; }
        public RoleType Role { get; set; }
    }
}
