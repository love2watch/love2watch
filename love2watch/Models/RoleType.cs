﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class RoleType
    {
        public RoleType()
        {
            CastInfo = new HashSet<CastInfo>();
        }

        public int Id { get; set; }
        public string Role { get; set; }

        public ICollection<CastInfo> CastInfo { get; set; }
    }
}
