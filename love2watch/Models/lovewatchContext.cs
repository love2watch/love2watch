﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Reflection;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq;

namespace love2watch.Models
{
    public partial class lovewatchContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<AkaName> AkaName { get; set; }
        public virtual DbSet<AkaTitle> AkaTitle { get; set; }
        public virtual DbSet<CastInfo> CastInfo { get; set; }
        public virtual DbSet<CharName> CharName { get; set; }
        public virtual DbSet<CompanyName> CompanyName { get; set; }
        public virtual DbSet<CompanyType> CompanyType { get; set; }
        public virtual DbSet<CompCastType> CompCastType { get; set; }
        public virtual DbSet<CompleteCast> CompleteCast { get; set; }
        public virtual DbSet<InfoType> InfoType { get; set; }
        public virtual DbSet<Keyword> Keyword { get; set; }
        public virtual DbSet<KindType> KindType { get; set; }
        public virtual DbSet<LinkType> LinkType { get; set; }
        public virtual DbSet<MovieCompanies> MovieCompanies { get; set; }
        public virtual DbSet<MovieInfo> MovieInfo { get; set; }
        public virtual DbSet<MovieInfoIdx> MovieInfoIdx { get; set; }
        public virtual DbSet<MovieKeyword> MovieKeyword { get; set; }
        public virtual DbSet<MovieLink> MovieLink { get; set; }
        public virtual DbSet<Name> Name { get; set; }
        public virtual DbSet<PersonInfo> PersonInfo { get; set; }
        public virtual DbSet<RoleType> RoleType { get; set; }
        public virtual DbSet<Title> Title { get; set; }
        public virtual DbSet<ApplicationUser> Profile { get; set; }

        public virtual DbSet<NameSearch> NameSearch { get; set; }
        public virtual DbSet<MovieSearch> MovieSearch { get; set; }

        public lovewatchContext(DbContextOptions<lovewatchContext> options) 
            : base(options)
        {
            // Uncomment this if migration fails with timeout error.
            //Database.SetCommandTimeout(300000);
            if (this.IsDesignTime())
            {
                // I don't know how clean is this solution
                // But at least there is no need to load appsettings twice or
                // comment and uncomment the code
                Database.SetCommandTimeout(3600);
            }
        }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql(@"Persist Security Info=True;Password=example;Username=imdb;Database=lovewatch;Host=localhost");
//            }
//        }

        protected void OnCreatingSearchView(ModelBuilder modelBuilder)
        {
            /**
             * Method to setup views for Full text search.
             * Extra function to separate from scaffolded methods in `OnModelCreating`
             */
            modelBuilder.Entity<NameSearch>(entity =>
            {
                entity.HasIndex(e => e.AllNamesVector)
                    .HasName("idx_fts_search")
                    .ForNpgsqlHasMethod("GIN");

                entity.Property<int?>("rank");
            });

            modelBuilder.Entity<MovieSearch>(entity => {
                entity.HasIndex(e => e.AllNamesVector)
                    .HasName("idx_fts_movies")
                    .ForNpgsqlHasMethod("GIN");

                entity.Property<int?>("rank");  // Hidden property for sorting
            });
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            OnCreatingSearchView(modelBuilder);
            
            modelBuilder.Entity<AkaName>(entity =>
            {
                entity.ToTable("aka_name");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("aka_name_idx_md5");

                entity.HasIndex(e => e.NamePcodeCf)
                    .HasName("aka_name_idx_pcodecf");

                entity.HasIndex(e => e.NamePcodeNf)
                    .HasName("aka_name_idx_pcodenf");

                entity.HasIndex(e => e.PersonId)
                    .HasName("aka_name_idx_person");

                entity.HasIndex(e => e.SurnamePcode)
                    .HasName("aka_name_idx_pcode");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ImdbIndex).HasColumnName("imdb_index");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.NamePcodeCf).HasColumnName("name_pcode_cf");

                entity.Property(e => e.NamePcodeNf).HasColumnName("name_pcode_nf");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.SurnamePcode).HasColumnName("surname_pcode");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.AkaName)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_id_exists");
            });

            modelBuilder.Entity<AkaTitle>(entity =>
            {
                entity.ToTable("aka_title");

                entity.HasIndex(e => e.EpisodeOfId)
                    .HasName("aka_title_idx_epof");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("aka_title_idx_md5");

                entity.HasIndex(e => e.MovieId)
                    .HasName("aka_title_idx_movieid");

                entity.HasIndex(e => e.PhoneticCode)
                    .HasName("aka_title_idx_pcode");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EpisodeNr).HasColumnName("episode_nr");

                entity.Property(e => e.EpisodeOfId).HasColumnName("episode_of_id");

                entity.Property(e => e.ImdbIndex).HasColumnName("imdb_index");

                entity.Property(e => e.KindId).HasColumnName("kind_id");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.PhoneticCode).HasColumnName("phonetic_code");

                entity.Property(e => e.ProductionYear).HasColumnName("production_year");

                entity.Property(e => e.SeasonNr).HasColumnName("season_nr");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");
            });

            modelBuilder.Entity<CastInfo>(entity =>
            {
                entity.ToTable("cast_info");

                entity.HasIndex(e => e.MovieId)
                    .HasName("cast_info_idx_mid");

                entity.HasIndex(e => e.PersonId)
                    .HasName("cast_info_idx_pid");

                entity.HasIndex(e => e.PersonRoleId)
                    .HasName("cast_info_idx_cid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.NrOrder).HasColumnName("nr_order");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.Property(e => e.PersonRoleId).HasColumnName("person_role_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.CastInfo)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.CastInfo)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_id_exists");

                entity.HasOne(d => d.PersonRole)
                    .WithMany(p => p.CastInfo)
                    .HasForeignKey(d => d.PersonRoleId)
                    .HasConstraintName("person_role_id_exists");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.CastInfo)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("role_id_exists");
            });

            modelBuilder.Entity<CharName>(entity =>
            {
                entity.ToTable("char_name");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("char_name_idx_md5");

                entity.HasIndex(e => e.Name)
                    .HasName("char_name_idx_name");

                entity.HasIndex(e => e.NamePcodeNf)
                    .HasName("char_name_idx_pcodenf");

                entity.HasIndex(e => e.SurnamePcode)
                    .HasName("char_name_idx_pcode");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ImdbId).HasColumnName("imdb_id");

                entity.Property(e => e.ImdbIndex).HasColumnName("imdb_index");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.NamePcodeNf).HasColumnName("name_pcode_nf");

                entity.Property(e => e.SurnamePcode).HasColumnName("surname_pcode");
            });

            modelBuilder.Entity<CompanyName>(entity =>
            {
                entity.ToTable("company_name");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("company_name_idx_md5");

                entity.HasIndex(e => e.Name)
                    .HasName("company_name_idx_name");

                entity.HasIndex(e => e.NamePcodeNf)
                    .HasName("company_name_idx_pcodenf");

                entity.HasIndex(e => e.NamePcodeSf)
                    .HasName("company_name_idx_pcodesf");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CountryCode).HasColumnName("country_code");

                entity.Property(e => e.ImdbId).HasColumnName("imdb_id");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.NamePcodeNf).HasColumnName("name_pcode_nf");

                entity.Property(e => e.NamePcodeSf).HasColumnName("name_pcode_sf");
            });

            modelBuilder.Entity<CompanyType>(entity =>
            {
                entity.ToTable("company_type");

                entity.HasIndex(e => e.Kind)
                    .HasName("company_type_kind_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Kind)
                    .IsRequired()
                    .HasColumnName("kind");
            });

            modelBuilder.Entity<CompCastType>(entity =>
            {
                entity.ToTable("comp_cast_type");

                entity.HasIndex(e => e.Kind)
                    .HasName("comp_cast_type_kind_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Kind)
                    .IsRequired()
                    .HasColumnName("kind");
            });

            modelBuilder.Entity<CompleteCast>(entity =>
            {
                entity.ToTable("complete_cast");

                entity.HasIndex(e => e.MovieId)
                    .HasName("complete_cast_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.SubjectId).HasColumnName("subject_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.CompleteCast)
                    .HasForeignKey(d => d.MovieId)
                    .HasConstraintName("movie_id_exists");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CompleteCastStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("status_id_exists");

                entity.HasOne(d => d.Subject)
                    .WithMany(p => p.CompleteCastSubject)
                    .HasForeignKey(d => d.SubjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("subject_id_exists");
            });

            modelBuilder.Entity<InfoType>(entity =>
            {
                entity.ToTable("info_type");

                entity.HasIndex(e => e.Info)
                    .HasName("info_type_info_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info");
            });

            modelBuilder.Entity<Keyword>(entity =>
            {
                entity.ToTable("keyword");

                entity.HasIndex(e => e.Keyword1)
                    .HasName("keyword_idx_keyword");

                entity.HasIndex(e => e.PhoneticCode)
                    .HasName("keyword_idx_pcode");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Keyword1)
                    .IsRequired()
                    .HasColumnName("keyword");

                entity.Property(e => e.PhoneticCode).HasColumnName("phonetic_code");
            });

            modelBuilder.Entity<KindType>(entity =>
            {
                entity.ToTable("kind_type");

                entity.HasIndex(e => e.Kind)
                    .HasName("kind_type_kind_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Kind)
                    .IsRequired()
                    .HasColumnName("kind");
            });

            modelBuilder.Entity<LinkType>(entity =>
            {
                entity.ToTable("link_type");

                entity.HasIndex(e => e.Link)
                    .HasName("link_type_link_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnName("link");
            });

            modelBuilder.Entity<MovieCompanies>(entity =>
            {
                entity.ToTable("movie_companies");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("movie_companies_idx_cid");

                entity.HasIndex(e => e.MovieId)
                    .HasName("movie_companies_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.CompanyTypeId).HasColumnName("company_type_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.MovieCompanies)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("company_id_exists");

                entity.HasOne(d => d.CompanyType)
                    .WithMany(p => p.MovieCompanies)
                    .HasForeignKey(d => d.CompanyTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("company_type_id_exists");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieCompanies)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");
            });

            modelBuilder.Entity<MovieInfo>(entity =>
            {
                entity.ToTable("movie_info");

                entity.HasIndex(e => e.MovieId)
                    .HasName("movie_info_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info");

                entity.Property(e => e.InfoTypeId).HasColumnName("info_type_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.HasOne(d => d.InfoType)
                    .WithMany(p => p.MovieInfo)
                    .HasForeignKey(d => d.InfoTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("info_type_id_exists");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieInfo)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");
            });

            modelBuilder.Entity<MovieInfoIdx>(entity =>
            {
                entity.ToTable("movie_info_idx");

                entity.HasIndex(e => e.Info)
                    .HasName("movie_info_idx_idx_info");

                entity.HasIndex(e => e.InfoTypeId)
                    .HasName("movie_info_idx_idx_infotypeid");

                entity.HasIndex(e => e.MovieId)
                    .HasName("movie_info_idx_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info");

                entity.Property(e => e.InfoTypeId).HasColumnName("info_type_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.HasOne(d => d.InfoType)
                    .WithMany(p => p.MovieInfoIdx)
                    .HasForeignKey(d => d.InfoTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("info_type_id_exists");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieInfoIdx)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");
            });

            modelBuilder.Entity<MovieKeyword>(entity =>
            {
                entity.ToTable("movie_keyword");

                entity.HasIndex(e => e.KeywordId)
                    .HasName("movie_keyword_idx_keywordid");

                entity.HasIndex(e => e.MovieId)
                    .HasName("movie_keyword_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.KeywordId).HasColumnName("keyword_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Keyword)
                    .WithMany(p => p.MovieKeyword)
                    .HasForeignKey(d => d.KeywordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("keyword_id_exists");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieKeyword)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");
            });

            modelBuilder.Entity<MovieLink>(entity =>
            {
                entity.ToTable("movie_link");

                entity.HasIndex(e => e.MovieId)
                    .HasName("movie_link_idx_mid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LinkTypeId).HasColumnName("link_type_id");

                entity.Property(e => e.LinkedMovieId).HasColumnName("linked_movie_id");

                entity.Property(e => e.MovieId).HasColumnName("movie_id");

                entity.HasOne(d => d.Movie)
                    .WithMany(p => p.MovieLink)
                    .HasForeignKey(d => d.MovieId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("movie_id_exists");
            });

            modelBuilder.Entity<Name>(entity =>
            {
                entity.ToTable("name");

                entity.HasIndex(e => e.ImdbId)
                    .HasName("name_idx_imdb_id");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("name_idx_md5");

                entity.HasIndex(e => e.Name1)
                    .HasName("name_idx_name");

                entity.HasIndex(e => e.NamePcodeCf)
                    .HasName("name_idx_pcodecf");

                entity.HasIndex(e => e.NamePcodeNf)
                    .HasName("name_idx_pcodenf");

                entity.HasIndex(e => e.SurnamePcode)
                    .HasName("name_idx_pcode");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.ImdbId).HasColumnName("imdb_id");

                entity.Property(e => e.ImdbIndex).HasColumnName("imdb_index");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.Name1)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.NamePcodeCf).HasColumnName("name_pcode_cf");

                entity.Property(e => e.NamePcodeNf).HasColumnName("name_pcode_nf");

                entity.Property(e => e.SurnamePcode).HasColumnName("surname_pcode");
            });

            modelBuilder.Entity<PersonInfo>(entity =>
            {
                entity.ToTable("person_info");

                entity.HasIndex(e => e.PersonId)
                    .HasName("person_info_idx_pid");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info");

                entity.Property(e => e.InfoTypeId).HasColumnName("info_type_id");

                entity.Property(e => e.Note).HasColumnName("note");

                entity.Property(e => e.PersonId).HasColumnName("person_id");

                entity.HasOne(d => d.InfoType)
                    .WithMany(p => p.PersonInfo)
                    .HasForeignKey(d => d.InfoTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("info_type_id_exists");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonInfo)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_id_exists");
            });

            modelBuilder.Entity<RoleType>(entity =>
            {
                entity.ToTable("role_type");

                entity.HasIndex(e => e.Role)
                    .HasName("role_type_role_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role");
            });

            modelBuilder.Entity<Title>(entity =>
            {
                entity.ToTable("title");

                entity.HasIndex(e => e.EpisodeNr)
                    .HasName("title_idx_episode_nr");

                entity.HasIndex(e => e.EpisodeOfId)
                    .HasName("title_idx_epof");

                entity.HasIndex(e => e.ImdbId)
                    .HasName("title_idx_imdb_id");

                entity.HasIndex(e => e.Md5sum)
                    .HasName("title_idx_md5");

                entity.HasIndex(e => e.PhoneticCode)
                    .HasName("title_idx_pcode");

                entity.HasIndex(e => e.SeasonNr)
                    .HasName("title_idx_season_nr");

                entity.HasIndex(e => e.Title1)
                    .HasName("title_idx_title");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EpisodeNr).HasColumnName("episode_nr");

                entity.Property(e => e.EpisodeOfId).HasColumnName("episode_of_id");

                entity.Property(e => e.ImdbId).HasColumnName("imdb_id");

                entity.Property(e => e.ImdbIndex).HasColumnName("imdb_index");

                entity.Property(e => e.KindId).HasColumnName("kind_id");

                entity.Property(e => e.Md5sum).HasColumnName("md5sum");

                entity.Property(e => e.PhoneticCode).HasColumnName("phonetic_code");

                entity.Property(e => e.ProductionYear).HasColumnName("production_year");

                entity.Property(e => e.SeasonNr).HasColumnName("season_nr");

                entity.Property(e => e.SeriesYears).HasColumnName("series_years");

                entity.Property(e => e.Title1)
                    .IsRequired()
                    .HasColumnName("title");

                entity.HasOne(d => d.EpisodeOf)
                    .WithMany(p => p.InverseEpisodeOf)
                    .HasForeignKey(d => d.EpisodeOfId)
                    .HasConstraintName("episode_of_id_exists");

                entity.HasOne(d => d.Kind)
                    .WithMany(p => p.Title)
                    .HasForeignKey(d => d.KindId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("kind_id_exists");

             });
        }
    }
    /**
     * Helper class to allow print of SQL for debug 
     */
    public static class IQueryableExtensions
    {
        private static readonly TypeInfo QueryCompilerTypeInfo = typeof(Microsoft.EntityFrameworkCore.Query.Internal.QueryCompiler).GetTypeInfo();

        private static readonly FieldInfo QueryCompilerField = typeof(Microsoft.EntityFrameworkCore.Query.Internal.EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");

        private static readonly PropertyInfo NodeTypeProviderField = QueryCompilerTypeInfo.DeclaredProperties.Single(x => x.Name == "NodeTypeProvider");

        private static readonly MethodInfo CreateQueryParserMethod = QueryCompilerTypeInfo.DeclaredMethods.First(x => x.Name == "CreateQueryParser");

        private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");

        private static readonly PropertyInfo DatabaseDependenciesField
            = typeof(Database).GetTypeInfo().DeclaredProperties.Single(x => x.Name == "Dependencies");

        public static string ToSql<TEntity>(this IQueryable<TEntity> query) where TEntity : class
        {
            if (!(query is Microsoft.EntityFrameworkCore.Query.Internal.EntityQueryable<TEntity>) && !(query is Microsoft.EntityFrameworkCore.Internal.InternalDbSet<TEntity>))
            {
                throw new ArgumentException("Invalid query");
            }

            var queryCompiler = (Microsoft.EntityFrameworkCore.Query.Internal.IQueryCompiler)QueryCompilerField.GetValue(query.Provider);
            var nodeTypeProvider = (Remotion.Linq.Parsing.Structure.INodeTypeProvider)NodeTypeProviderField.GetValue(queryCompiler);
            var parser = (Remotion.Linq.Parsing.Structure.IQueryParser)CreateQueryParserMethod.Invoke(queryCompiler, new object[] { nodeTypeProvider });
            var queryModel = parser.GetParsedQuery(query.Expression);
            var database = DataBaseField.GetValue(queryCompiler);
            var queryCompilationContextFactory = ((DatabaseDependencies)DatabaseDependenciesField.GetValue(database)).QueryCompilationContextFactory;
            var queryCompilationContext = queryCompilationContextFactory.Create(false);
            var modelVisitor = (Microsoft.EntityFrameworkCore.Query.RelationalQueryModelVisitor)queryCompilationContext.CreateQueryModelVisitor();
            modelVisitor.CreateQueryExecutor<TEntity>(queryModel);
            var sql = modelVisitor.Queries.First().ToString();

            return sql;
        }
    }
}
