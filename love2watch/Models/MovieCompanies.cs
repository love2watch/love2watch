﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class MovieCompanies
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int CompanyId { get; set; }
        public int CompanyTypeId { get; set; }
        public string Note { get; set; }

        public CompanyName Company { get; set; }
        public CompanyType CompanyType { get; set; }
        public Title Movie { get; set; }
    }
}
