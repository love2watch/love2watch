﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class KindType
    {
        public KindType()
        {
            Title = new HashSet<Title>();
        }

        public int Id { get; set; }
        public string Kind { get; set; }

        public ICollection<Title> Title { get; set; }
    }
}
