﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CompleteCast
    {
        public int Id { get; set; }
        public int? MovieId { get; set; }
        public int SubjectId { get; set; }
        public int StatusId { get; set; }

        public Title Movie { get; set; }
        public CompCastType Status { get; set; }
        public CompCastType Subject { get; set; }
    }
}
