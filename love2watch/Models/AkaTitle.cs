﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class AkaTitle
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string ImdbIndex { get; set; }
        public int KindId { get; set; }
        public int? ProductionYear { get; set; }
        public string PhoneticCode { get; set; }
        public int? EpisodeOfId { get; set; }
        public int? SeasonNr { get; set; }
        public int? EpisodeNr { get; set; }
        public string Note { get; set; }
        public string Md5sum { get; set; }
    }
}
