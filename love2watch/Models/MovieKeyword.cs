﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class MovieKeyword
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int KeywordId { get; set; }

        public Keyword Keyword { get; set; }
        public Title Movie { get; set; }
    }
}
