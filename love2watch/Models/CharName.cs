﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CharName
    {
        public CharName()
        {
            CastInfo = new HashSet<CastInfo>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ImdbIndex { get; set; }
        public int? ImdbId { get; set; }
        public string NamePcodeNf { get; set; }
        public string SurnamePcode { get; set; }
        public string Md5sum { get; set; }

        public ICollection<CastInfo> CastInfo { get; set; }
    }
}
