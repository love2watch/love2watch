﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class Name
    {
        public Name()
        {
            AkaName = new HashSet<AkaName>();
            CastInfo = new HashSet<CastInfo>();
            PersonInfo = new HashSet<PersonInfo>();
        }

        public int Id { get; set; }
        public string Name1 { get; set; }
        public string ImdbIndex { get; set; }
        public int? ImdbId { get; set; }
        public string Gender { get; set; }
        public string NamePcodeCf { get; set; }
        public string NamePcodeNf { get; set; }
        public string SurnamePcode { get; set; }
        public string Md5sum { get; set; }

        public ICollection<AkaName> AkaName { get; set; }
        public ICollection<CastInfo> CastInfo { get; set; }
        public ICollection<PersonInfo> PersonInfo { get; set; }
    }
}
