﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class MovieInfo
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int InfoTypeId { get; set; }
        public string Info { get; set; }
        public string Note { get; set; }

        public InfoType InfoType { get; set; }
        public Title Movie { get; set; }
    }
}
