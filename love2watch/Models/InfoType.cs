﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class InfoType
    {
        public InfoType()
        {
            MovieInfo = new HashSet<MovieInfo>();
            MovieInfoIdx = new HashSet<MovieInfoIdx>();
            PersonInfo = new HashSet<PersonInfo>();
        }

        public int Id { get; set; }
        public string Info { get; set; }

        public ICollection<MovieInfo> MovieInfo { get; set; }
        public ICollection<MovieInfoIdx> MovieInfoIdx { get; set; }
        public ICollection<PersonInfo> PersonInfo { get; set; }
    }
}
