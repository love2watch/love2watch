﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class AkaName
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string ImdbIndex { get; set; }
        public string NamePcodeCf { get; set; }
        public string NamePcodeNf { get; set; }
        public string SurnamePcode { get; set; }
        public string Md5sum { get; set; }

        public Name Person { get; set; }
    }
}
