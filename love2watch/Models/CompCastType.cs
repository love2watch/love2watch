﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class CompCastType
    {
        public CompCastType()
        {
            CompleteCastStatus = new HashSet<CompleteCast>();
            CompleteCastSubject = new HashSet<CompleteCast>();
        }

        public int Id { get; set; }
        public string Kind { get; set; }

        public ICollection<CompleteCast> CompleteCastStatus { get; set; }
        public ICollection<CompleteCast> CompleteCastSubject { get; set; }
    }
}
