﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class MovieLink
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public int LinkedMovieId { get; set; }
        public int LinkTypeId { get; set; }

        public Title Movie { get; set; }
    }
}
