﻿using System;
using System.Collections.Generic;

namespace love2watch.Models
{
    public partial class Title
    {
        public Title()
        {
            CastInfo = new HashSet<CastInfo>();
            CompleteCast = new HashSet<CompleteCast>();
            InverseEpisodeOf = new HashSet<Title>();
            MovieCompanies = new HashSet<MovieCompanies>();
            MovieInfo = new HashSet<MovieInfo>();
            MovieInfoIdx = new HashSet<MovieInfoIdx>();
            MovieKeyword = new HashSet<MovieKeyword>();
            MovieLink = new HashSet<MovieLink>();
        }

        public int Id { get; set; }
        public string Title1 { get; set; }
        public string ImdbIndex { get; set; }
        public int KindId { get; set; }
        public int? ProductionYear { get; set; }
        public int? ImdbId { get; set; }
        public string PhoneticCode { get; set; }
        public int? EpisodeOfId { get; set; }
        public int? SeasonNr { get; set; }
        public int? EpisodeNr { get; set; }
        public string SeriesYears { get; set; }
        public string Md5sum { get; set; }

        public Title EpisodeOf { get; set; }
        public KindType Kind { get; set; }
        public ICollection<CastInfo> CastInfo { get; set; }
        public ICollection<CompleteCast> CompleteCast { get; set; }
        public ICollection<Title> InverseEpisodeOf { get; set; }
        public ICollection<MovieCompanies> MovieCompanies { get; set; }
        public ICollection<MovieInfo> MovieInfo { get; set; }
        public ICollection<MovieInfoIdx> MovieInfoIdx { get; set; }
        public ICollection<MovieKeyword> MovieKeyword { get; set; }
        public ICollection<MovieLink> MovieLink { get; set; }
    }
}
