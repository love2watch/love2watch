﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LinqKit;
using love2watch.Models;

namespace love2watch.Controllers
{
    [Produces("application/json")]
    [Route("api/Names")]
    public class NamesController : Controller
    {
        private readonly lovewatchContext _context;

        public NamesController(lovewatchContext context)
        {
            _context = context;
        }

        // GET: api/Names
        /// <summary>
        ///     Paginate through all <see cref="Name"/>
        /// </summary>
        /// <param name="page">Number of page</param>
        /// <returns>List of 25 actors</returns>
        [HttpGet]
        public IEnumerable<Name> GetName([FromQuery] int page = 0)
        {
            IQueryable<Name> result = _context.Name.OrderBy(n => n.Name1);
            if (page > 0)
            {
                result = result.Skip(page * 25);
            }
            result = result.Take(25);

            return result;
        }

        // GET: api/Names/5
        /// <summary>
        ///     Load Actor's or Director's details
        /// </summary>
        /// <param name="id">Int ID of person</param>
        /// <returns><see cref="Name"/></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetNameDetail([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var name = await _context.Name
                .Include(n => n.AkaName)
                //.Include(n => n.PersonInfo)
                //.Include(n => n.CastInfo)
                //.Select(n => new {
                //    n.Name1,
                //    n.Id,
                //    n.Gender,
                //    AkaNames = n.AkaName.Select(a => new { a.Name, a.Id }),
                //    n.PersonInfo
                //})
                .SingleOrDefaultAsync(m => m.Id == id);

            if (name == null)
            {
                return NotFound();
            }
            
            return Ok(name);
        }

        /// <summary>
        ///     Search persons by name
        /// </summary>
        /// <param name="search">Name of person</param>
        /// <param name="roleType">ID of role from <see cref="RoleType"/></param>
        /// <param name="movies">IDs of movies (<see cref="Title"/>)</param>
        /// <returns>List of persons <see cref="Name"/></returns>
        [HttpGet("s")]
        public async Task<IEnumerable<Name>> GetNameSearch(
            [FromQuery] string search="",
            [FromQuery] int roleType=0,
            [FromQuery] int[] movies=null   
        )
        {
            IQueryable<Name> result = _context.Name.AsExpandable();
            var castExpression = PredicateBuilder.New<CastInfo>(true);
            int[] actorRoles = {1, 2};

            // LINQKit fixes the shit.
            if (movies != null && movies.Length > 0)
            {
                castExpression = castExpression.And(c => movies.Contains(c.MovieId));
            }
            if (roleType > 0)
            {
                if (actorRoles.Contains(roleType))
                {
                    castExpression = castExpression.And(c => actorRoles.Contains(c.RoleId));
                }
                else
                {
                    castExpression = castExpression.And(c => c.RoleId == roleType);
                }
            }

            var castPredicate = castExpression.Build();
            result = result.Where(n => n.CastInfo.Any(castPredicate.Compile()));
            
            foreach (var s in search.Split(' '))
            {
                if (s.Length < 2)
                {
                    continue;
                }
                result = result.Where(n => n.Name1.Contains(s) || 
                                        n.AkaName.Any(a => a.Name.Contains(s)));
            }
            // This query is just too long
            // result = result.OrderByDescending(n => n.CastInfo.Count());
            return await result.OrderBy(n => n.Name1).Take(25).ToListAsync();
        }

        /// <summary>
        ///     List top-10 actors by selected movies.
        ///     Ordered by number of matches of movies
        /// </summary>
        /// <param name="movies"><see cref="Title"/></param>
        /// <param name="roles"><see cref="RoleType"/></param>
        /// <returns>
        ///     list of dicts 
        ///     { person: "<see cref="Name"/> with <see cref="CastInfo"/>",
        ///       movies: [ IDs of <see cref="Title"/> ]}
        /// </returns>
        [HttpGet("bymovie")]
        public async Task<ActionResult> GetNameByMovie(
            [FromQuery] int[] movies=null,
            [FromQuery] int[] roles=null)
        {
            if (movies == null || roles == null)
            {
                return Ok(new List<CastInfo> { });
            }

            var persons = await _context.CastInfo.AsNoTracking()
                .Where(c => movies.Contains(c.MovieId))
                .Where(c => roles.Contains(c.RoleId))
                .Where(c => c.NrOrder <= 10)
                .OrderBy(c => c.PersonId)
                .Include(c => c.Person)
                .ToListAsync();

            return Ok(persons
                .GroupBy(p => p.Person)
                .Select(g => new { person = g.Key, movies = g.Select(c => c.MovieId) })
                .OrderByDescending(g => g.movies.Count())
                    .ThenBy(g => g.person.CastInfo.Min(c => c.NrOrder)));
        }

        /// <summary>
        ///     Search persons by name, but uses View with full-text-search index
        /// </summary>
        /// <param name="search">Name of person</param>
        /// <param name="role">ID of role from <see cref="RoleType"/></param>
        /// <param name="movies">IDs of movies (<see cref="Title"/>)</param>
        /// <returns>List of persons <see cref="Name"/></returns>
        [HttpGet("fulltext")]
        public IEnumerable<Name> GetFullTextSearch(
            [FromQuery] string search="", 
            [FromQuery] int role=0, 
            [FromQuery] int[] movies=null)
        {
            /**
             * Example action to show how to use FullTextSearch views
             */ 
            IQueryable<NameSearch> data = _context.NameSearch.AsNoTracking();
            int[] actorRoles = {1, 2};
            int[] searchRoles;

            if (role > 0 && actorRoles.Contains(role))
            {
                searchRoles = actorRoles;
            }
            else
            {
                searchRoles = new int[] {role};
            }

            var sql = @"SELECT *, ts_rank(all_names_vector, plainto_tsquery({0})) as rank 
                FROM name_and_aka 
                WHERE all_names_vector @@ plainto_tsquery({0})";

            if (role > 0)
            {
                // this amount of curly brackets is not an error.
                // It's double escaping, here and in FromSql
                // the resulting query should read: all_roles && '{1, 2}'
                sql += $" AND all_roles && '{{{{{string.Join(',', searchRoles)}}}}}'"; 
            }

            data = data.FromSql(sql, search);

            if (movies != null && movies.Length > 0)
            {
                // LINQKit fixes the shit.
                var castExpression = PredicateBuilder.New<CastInfo>(true);
                castExpression = castExpression.And(c => movies.Contains(c.MovieId));
                if (role > 0) 
                {
                    castExpression = castExpression.And(c => searchRoles.Contains(c.RoleId));
                }
                var castPredicate = castExpression.Build();
                data = data.AsExpandable().Where(s => s.RelatedPerson.CastInfo.Any(castPredicate.Compile()));
            }

            var nameData = data
                .OrderByDescending(b => EF.Property<int>(b, "rank"))
                    .ThenByDescending(b => b.RolesCount)
                .Select(p => p.RelatedPerson)
                .Take(25);

            // Console.WriteLine(nameData.ToSql());
            return nameData;
        }

        private bool NameExists(int id)
        {
            return _context.Name.Any(e => e.Id == id);
        }

    }
}