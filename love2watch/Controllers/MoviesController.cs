﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using love2watch.Models;

namespace love2watch.Controllers
{
    [Produces("application/json")]
    [Route("api/Movies")]
    public class MoviesController : Controller
    {
        private readonly lovewatchContext _context;

        public MoviesController(lovewatchContext context)
        {
            _context = context;
        }

        // GET: api/Movies
        /// <summary>
        ///     Paginate through all <see cref="Title"/>
        /// </summary>
        /// <param name="page">0-based page index</param>
        /// <returns>List of 25 <see cref="Title"/></returns>
        [HttpGet]
        public IQueryable<Title> GetTitle([FromQuery] int page = 0)
        {
            var titles = _context.Title.AsQueryable();
            if (page > 0)
            {
                titles = titles.Skip(page * 25);
            }
            return titles.OrderBy(t => t.Title1).Take(25);
        }

        // GET: api/Movies/5
        /// <summary>
        ///     Movie details page
        /// </summary>
        /// <param name="id">ID of <see cref="Title"/></param>
        /// <returns><see cref="Title"/> with <see cref="KindType"/> loaded</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTitleDetail([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var title = await _context.Title
                .Include("Kind")
                .SingleOrDefaultAsync(m => m.Id == id);
            await _context.Entry(title).Reference<KindType>("Kind").LoadAsync();

            if (title == null)
            {
                return NotFound();
            }

            return Ok(title);
        }

        /// <summary>
        ///     Movie search by name. 
        /// </summary>
        /// <remarks>
        ///     Forces MovieType=1 (Movies)
        /// </remarks>
        /// <param name="name">Movie name</param>
        /// <todo>Full Text Search</todo>
        /// <returns>List of <see cref="Title"/></returns>
        [HttpGet("byname")]
        public async Task<IEnumerable<Title>> GetTitleByName([FromQuery] string name)
        {
            return await _context.Title
                .Where(t => t.Title1.ToLower().Contains(name.ToLower()))
                .Where(t => t.KindId == 1)  // Movie only
                .OrderBy(t => t.Title1)
                .Take(25)
                .ToListAsync();
        }

        /// <summary>
        ///     Movie search by names, uses Full text search from view
        /// </summary>
        /// <param name="name">Movie name</param>
        /// <returns>List of <see cref="Title"/></returns>
        [HttpGet("fulltext")]
        public async Task<IEnumerable<Title>> GetTitleBySearch([FromQuery] string name)
        {
            return await _context.MovieSearch.AsNoTracking()
                .FromSql($"SELECT *, ts_rank(all_names_vector, plainto_tsquery({name})) as rank FROM title_and_aka WHERE all_names_vector @@ plainto_tsquery({name})")
                .OrderByDescending(s => EF.Property<int>(s, "rank"))
                .Select(s => s.RelatedTitle)
                .Where(t => t.KindId == 1) // Movie only
                .ToListAsync();
        }

        /// <summary>
        ///     Movie search by person (actor or director etc)
        /// </summary>
        /// <example>
        /// -- Pulp fiction: 4014846
        /// -- Bruce Willis: 2580071
        /// -- John Travolta: 2408852
        /// -- Samuel L Jackson: 1117451
        /// -- Tarantino, Quentin: 2345427
        /// </example>
        /// <param name="persons">IDs of person from <see cref="Name"/></param>
        /// <returns>
        ///     Dict with 2 keys 
        ///     {
        ///         "movie": <see cref="Title"/>,
        ///         "matches: [ {person: <see cref="Name"/>, role: <see cref="CharName"/>, position: <see cref="RoleType" />}, ... ]
        /// </returns>
        [HttpGet("byperson")]
        public async Task<IActionResult> GetTitleByPerson([FromQuery] int[] persons)
        {
            /** For testing
             * -- Pulp fiction: 4014846
             * -- Bruce Willis: 2580071
             * -- John Travolta: 2408852
             * -- Samuel L Jackson: 1117451
             * -- Tarantino, Quentin: 2345427
             */

            // I can't make it with LINQ due to limited GroupBy support
            // We either do sorting in ORM in c#, or raw query
            // TODO: it's terrible to build SQL like this, 
            // We need to find out how to pass an array
            var rawSql = @"SELECT m.*
                FROM ""cast_info"" as c
                INNER JOIN ""title"" as m ON c.movie_id = m.id
                WHERE (m.kind_id = 1) AND c.person_id IN (" + string.Join(",", persons) + @") 
                GROUP BY m.id
                ORDER BY COUNT(DISTINCT c.person_id) DESC, bool_or(c.role_id = 8) DESC, m.production_year DESC
                LIMIT 25";
            var result = (await _context.Title
                .FromSql(rawSql)
                // .Take(25)
                .SelectMany(
                    t => t.CastInfo.Where(c => persons.Contains(c.PersonId)).Select(c => new { person = c.Person.Name1, role = c.PersonRole.Name, position = c.Role.Role }),
                    (p, c) => new { movie = p, matches = c }
                )
                .ToListAsync()
                )
                .GroupBy(r => r.movie)
                .Select(g => new { movie = g.Key, matches = g.Select(m => m.matches) });

            return Ok(result);
        }

        private bool TitleExists(int id)
        {
            return _context.Title.Any(e => e.Id == id);
        }
    }
}