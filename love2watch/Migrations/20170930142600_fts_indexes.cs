﻿using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace love2watch.Migrations
{
    public partial class fts_indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /// This migration creates materialized views to use them as indexes for text search
            /// Note for devs - you can uncomment the WHERE to have less indexed data
            migrationBuilder.Sql(
                @"CREATE MATERIALIZED VIEW name_and_aka AS 
                   SELECT t1.id,
                    t1.name,
                    (setweight(to_tsvector(t1.name), 'A') || setweight(to_tsvector(COALESCE(string_agg(t2.name, ' '), '')), 'B')) AS all_names_vector,
                    NULLIF(ARRAY_AGG(DISTINCT t3.role_id), '{NULL}') AS all_roles,
                    COUNT(t3.role_id) as roles_count
                   FROM(name t1
                     LEFT JOIN aka_name t2 ON t2.person_id = t1.id
                     LEFT JOIN cast_info t3 ON t3.person_id = t1.id
                    )
                   WHERE(t1.name LIKE 'W%')
                   GROUP BY t1.id;"
                );
            //migrationBuilder.CreateTable(
            //    name: "name_and_aka",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(type: "int4", nullable: false),
            //        all_names_vector = table.Column<NpgsqlTsVector>(type: "tsvector", nullable: true),
            //        all_roles = table.Column<int[]>(type: "int4[]", nullable: true),
            //        name = table.Column<string>(type: "text", nullable: true),
            //        roles_count = table.Column<int>(type: "int4", nullable: false),
            //        rank = table.Column<int>(type: "int4", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_name_and_aka", x => x.id);
            //        table.ForeignKey(
            //            name: "FK_name_and_aka_name_id",
            //            column: x => x.id,
            //            principalTable: "name",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.Sql(
                @"CREATE MATERIALIZED VIEW title_and_aka AS 
                    SELECT t1.id,
                        t1.title,
                        (setweight(to_tsvector(t1.title), 'A') || setweight(to_tsvector(COALESCE(string_agg(t2.title, ' '), '')), 'B')) AS all_names_vector
                    FROM(title t1
                         LEFT JOIN aka_title t2 ON (t2.movie_id = t1.id))
                    WHERE t1.kind_id = 1
                            AND t1.title LIKE 'P%'
                    GROUP BY t1.id; "
                );
            //migrationBuilder.CreateTable(
            //    name: "title_and_aka",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(type: "int4", nullable: false),
            //        all_names_vector = table.Column<NpgsqlTsVector>(type: "tsvector", nullable: true),
            //        name = table.Column<string>(type: "text", nullable: true),
            //        rank = table.Column<int>(type: "int4", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_title_and_aka", x => x.id);
            //        table.ForeignKey(
            //            name: "FK_title_and_aka_title_id",
            //            column: x => x.id,
            //            principalTable: "title",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateIndex(
                name: "idx_fts_search",
                table: "name_and_aka",
                column: "all_names_vector")
                .Annotation("Npgsql:Npgsql:IndexMethod", "GIN");

            migrationBuilder.CreateIndex(
                name: "idx_fts_movies",
                table: "title_and_aka",
                column: "all_names_vector")
                .Annotation("Npgsql:Npgsql:IndexMethod", "GIN");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP MATERIALIZED VIEW ""name_and_aka""");
            migrationBuilder.Sql(@"DROP MATERIALIZED VIEW ""title_and_aka""");

            //migrationBuilder.DropTable(
            //    name: "name_and_aka");

            //migrationBuilder.DropTable(
            //    name: "title_and_aka");
        }
    }
}
