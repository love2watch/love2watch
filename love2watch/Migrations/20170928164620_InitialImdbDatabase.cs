﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace love2watch.Migrations
{
    public partial class InitialImdbDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            Console.WriteLine("This migration does nothing, should be created from dump");
            return ;
            migrationBuilder.CreateTable(
                name: "aka_title",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    episode_nr = table.Column<int>(type: "int4", nullable: true),
                    episode_of_id = table.Column<int>(type: "int4", nullable: true),
                    imdb_index = table.Column<string>(type: "text", nullable: true),
                    kind_id = table.Column<int>(type: "int4", nullable: false),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    movie_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true),
                    phonetic_code = table.Column<string>(type: "text", nullable: true),
                    production_year = table.Column<int>(type: "int4", nullable: true),
                    season_nr = table.Column<int>(type: "int4", nullable: true),
                    title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_aka_title", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "char_name",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    imdb_id = table.Column<int>(type: "int4", nullable: true),
                    imdb_index = table.Column<string>(type: "text", nullable: true),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: false),
                    name_pcode_nf = table.Column<string>(type: "text", nullable: true),
                    surname_pcode = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_char_name", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "comp_cast_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    kind = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_comp_cast_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "company_name",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    country_code = table.Column<string>(type: "text", nullable: true),
                    imdb_id = table.Column<int>(type: "int4", nullable: true),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: false),
                    name_pcode_nf = table.Column<string>(type: "text", nullable: true),
                    name_pcode_sf = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_company_name", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "company_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    kind = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_company_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "info_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    info = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_info_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "keyword",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    keyword = table.Column<string>(type: "text", nullable: false),
                    phonetic_code = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_keyword", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "kind_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    kind = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_kind_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "link_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    link = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_link_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "name",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    gender = table.Column<string>(type: "text", nullable: true),
                    imdb_id = table.Column<int>(type: "int4", nullable: true),
                    imdb_index = table.Column<string>(type: "text", nullable: true),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: false),
                    name_pcode_cf = table.Column<string>(type: "text", nullable: true),
                    name_pcode_nf = table.Column<string>(type: "text", nullable: true),
                    surname_pcode = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_name", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "role_type",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    role = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_type", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "title",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    episode_nr = table.Column<int>(type: "int4", nullable: true),
                    episode_of_id = table.Column<int>(type: "int4", nullable: true),
                    imdb_id = table.Column<int>(type: "int4", nullable: true),
                    imdb_index = table.Column<string>(type: "text", nullable: true),
                    kind_id = table.Column<int>(type: "int4", nullable: false),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    phonetic_code = table.Column<string>(type: "text", nullable: true),
                    production_year = table.Column<int>(type: "int4", nullable: true),
                    season_nr = table.Column<int>(type: "int4", nullable: true),
                    series_years = table.Column<string>(type: "text", nullable: true),
                    title = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_title", x => x.id);
                    table.ForeignKey(
                        name: "episode_of_id_exists",
                        column: x => x.episode_of_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "kind_id_exists",
                        column: x => x.kind_id,
                        principalTable: "kind_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "aka_name",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    imdb_index = table.Column<string>(type: "text", nullable: true),
                    md5sum = table.Column<string>(type: "text", nullable: true),
                    name = table.Column<string>(type: "text", nullable: false),
                    name_pcode_cf = table.Column<string>(type: "text", nullable: true),
                    name_pcode_nf = table.Column<string>(type: "text", nullable: true),
                    person_id = table.Column<int>(type: "int4", nullable: false),
                    surname_pcode = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_aka_name", x => x.id);
                    table.ForeignKey(
                        name: "person_id_exists",
                        column: x => x.person_id,
                        principalTable: "name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "person_info",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    info = table.Column<string>(type: "text", nullable: false),
                    info_type_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true),
                    person_id = table.Column<int>(type: "int4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_person_info", x => x.id);
                    table.ForeignKey(
                        name: "info_type_id_exists",
                        column: x => x.info_type_id,
                        principalTable: "info_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "person_id_exists",
                        column: x => x.person_id,
                        principalTable: "name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cast_info",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    movie_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true),
                    nr_order = table.Column<int>(type: "int4", nullable: true),
                    person_id = table.Column<int>(type: "int4", nullable: false),
                    person_role_id = table.Column<int>(type: "int4", nullable: true),
                    role_id = table.Column<int>(type: "int4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cast_info", x => x.id);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "person_id_exists",
                        column: x => x.person_id,
                        principalTable: "name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "person_role_id_exists",
                        column: x => x.person_role_id,
                        principalTable: "char_name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "role_id_exists",
                        column: x => x.role_id,
                        principalTable: "role_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "complete_cast",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    movie_id = table.Column<int>(type: "int4", nullable: true),
                    status_id = table.Column<int>(type: "int4", nullable: false),
                    subject_id = table.Column<int>(type: "int4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_complete_cast", x => x.id);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "status_id_exists",
                        column: x => x.status_id,
                        principalTable: "comp_cast_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "subject_id_exists",
                        column: x => x.subject_id,
                        principalTable: "comp_cast_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "movie_companies",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    company_id = table.Column<int>(type: "int4", nullable: false),
                    company_type_id = table.Column<int>(type: "int4", nullable: false),
                    movie_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie_companies", x => x.id);
                    table.ForeignKey(
                        name: "company_id_exists",
                        column: x => x.company_id,
                        principalTable: "company_name",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "company_type_id_exists",
                        column: x => x.company_type_id,
                        principalTable: "company_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "movie_info",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    info = table.Column<string>(type: "text", nullable: false),
                    info_type_id = table.Column<int>(type: "int4", nullable: false),
                    movie_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie_info", x => x.id);
                    table.ForeignKey(
                        name: "info_type_id_exists",
                        column: x => x.info_type_id,
                        principalTable: "info_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "movie_info_idx",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    info = table.Column<string>(type: "text", nullable: false),
                    info_type_id = table.Column<int>(type: "int4", nullable: false),
                    movie_id = table.Column<int>(type: "int4", nullable: false),
                    note = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie_info_idx", x => x.id);
                    table.ForeignKey(
                        name: "info_type_id_exists",
                        column: x => x.info_type_id,
                        principalTable: "info_type",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "movie_keyword",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    keyword_id = table.Column<int>(type: "int4", nullable: false),
                    movie_id = table.Column<int>(type: "int4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie_keyword", x => x.id);
                    table.ForeignKey(
                        name: "keyword_id_exists",
                        column: x => x.keyword_id,
                        principalTable: "keyword",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "movie_link",
                columns: table => new
                {
                    id = table.Column<int>(type: "int4", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    link_type_id = table.Column<int>(type: "int4", nullable: false),
                    linked_movie_id = table.Column<int>(type: "int4", nullable: false),
                    movie_id = table.Column<int>(type: "int4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_movie_link", x => x.id);
                    table.ForeignKey(
                        name: "movie_id_exists",
                        column: x => x.movie_id,
                        principalTable: "title",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "aka_name_idx_md5",
                table: "aka_name",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "aka_name_idx_pcodecf",
                table: "aka_name",
                column: "name_pcode_cf");

            migrationBuilder.CreateIndex(
                name: "aka_name_idx_pcodenf",
                table: "aka_name",
                column: "name_pcode_nf");

            migrationBuilder.CreateIndex(
                name: "aka_name_idx_person",
                table: "aka_name",
                column: "person_id");

            migrationBuilder.CreateIndex(
                name: "aka_name_idx_pcode",
                table: "aka_name",
                column: "surname_pcode");

            migrationBuilder.CreateIndex(
                name: "aka_title_idx_epof",
                table: "aka_title",
                column: "episode_of_id");

            migrationBuilder.CreateIndex(
                name: "aka_title_idx_md5",
                table: "aka_title",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "aka_title_idx_movieid",
                table: "aka_title",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "aka_title_idx_pcode",
                table: "aka_title",
                column: "phonetic_code");

            migrationBuilder.CreateIndex(
                name: "cast_info_idx_mid",
                table: "cast_info",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "cast_info_idx_pid",
                table: "cast_info",
                column: "person_id");

            migrationBuilder.CreateIndex(
                name: "cast_info_idx_cid",
                table: "cast_info",
                column: "person_role_id");

            migrationBuilder.CreateIndex(
                name: "IX_cast_info_role_id",
                table: "cast_info",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "char_name_idx_md5",
                table: "char_name",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "char_name_idx_name",
                table: "char_name",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "char_name_idx_pcodenf",
                table: "char_name",
                column: "name_pcode_nf");

            migrationBuilder.CreateIndex(
                name: "char_name_idx_pcode",
                table: "char_name",
                column: "surname_pcode");

            migrationBuilder.CreateIndex(
                name: "comp_cast_type_kind_key",
                table: "comp_cast_type",
                column: "kind",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "company_name_idx_md5",
                table: "company_name",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "company_name_idx_name",
                table: "company_name",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "company_name_idx_pcodenf",
                table: "company_name",
                column: "name_pcode_nf");

            migrationBuilder.CreateIndex(
                name: "company_name_idx_pcodesf",
                table: "company_name",
                column: "name_pcode_sf");

            migrationBuilder.CreateIndex(
                name: "company_type_kind_key",
                table: "company_type",
                column: "kind",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "complete_cast_idx_mid",
                table: "complete_cast",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "IX_complete_cast_status_id",
                table: "complete_cast",
                column: "status_id");

            migrationBuilder.CreateIndex(
                name: "IX_complete_cast_subject_id",
                table: "complete_cast",
                column: "subject_id");

            migrationBuilder.CreateIndex(
                name: "info_type_info_key",
                table: "info_type",
                column: "info",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "keyword_idx_keyword",
                table: "keyword",
                column: "keyword");

            migrationBuilder.CreateIndex(
                name: "keyword_idx_pcode",
                table: "keyword",
                column: "phonetic_code");

            migrationBuilder.CreateIndex(
                name: "kind_type_kind_key",
                table: "kind_type",
                column: "kind",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "link_type_link_key",
                table: "link_type",
                column: "link",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "movie_companies_idx_cid",
                table: "movie_companies",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "IX_movie_companies_company_type_id",
                table: "movie_companies",
                column: "company_type_id");

            migrationBuilder.CreateIndex(
                name: "movie_companies_idx_mid",
                table: "movie_companies",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "IX_movie_info_info_type_id",
                table: "movie_info",
                column: "info_type_id");

            migrationBuilder.CreateIndex(
                name: "movie_info_idx_mid",
                table: "movie_info",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "movie_info_idx_idx_info",
                table: "movie_info_idx",
                column: "info");

            migrationBuilder.CreateIndex(
                name: "movie_info_idx_idx_infotypeid",
                table: "movie_info_idx",
                column: "info_type_id");

            migrationBuilder.CreateIndex(
                name: "movie_info_idx_idx_mid",
                table: "movie_info_idx",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "movie_keyword_idx_keywordid",
                table: "movie_keyword",
                column: "keyword_id");

            migrationBuilder.CreateIndex(
                name: "movie_keyword_idx_mid",
                table: "movie_keyword",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "movie_link_idx_mid",
                table: "movie_link",
                column: "movie_id");

            migrationBuilder.CreateIndex(
                name: "name_idx_imdb_id",
                table: "name",
                column: "imdb_id");

            migrationBuilder.CreateIndex(
                name: "name_idx_md5",
                table: "name",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "name_idx_name",
                table: "name",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "name_idx_pcodecf",
                table: "name",
                column: "name_pcode_cf");

            migrationBuilder.CreateIndex(
                name: "name_idx_pcodenf",
                table: "name",
                column: "name_pcode_nf");

            migrationBuilder.CreateIndex(
                name: "name_idx_pcode",
                table: "name",
                column: "surname_pcode");

            migrationBuilder.CreateIndex(
                name: "IX_person_info_info_type_id",
                table: "person_info",
                column: "info_type_id");

            migrationBuilder.CreateIndex(
                name: "person_info_idx_pid",
                table: "person_info",
                column: "person_id");

            migrationBuilder.CreateIndex(
                name: "role_type_role_key",
                table: "role_type",
                column: "role",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "title_idx_episode_nr",
                table: "title",
                column: "episode_nr");

            migrationBuilder.CreateIndex(
                name: "title_idx_epof",
                table: "title",
                column: "episode_of_id");

            migrationBuilder.CreateIndex(
                name: "title_idx_imdb_id",
                table: "title",
                column: "imdb_id");

            migrationBuilder.CreateIndex(
                name: "IX_title_kind_id",
                table: "title",
                column: "kind_id");

            migrationBuilder.CreateIndex(
                name: "title_idx_md5",
                table: "title",
                column: "md5sum");

            migrationBuilder.CreateIndex(
                name: "title_idx_pcode",
                table: "title",
                column: "phonetic_code");

            migrationBuilder.CreateIndex(
                name: "title_idx_season_nr",
                table: "title",
                column: "season_nr");

            migrationBuilder.CreateIndex(
                name: "title_idx_title",
                table: "title",
                column: "title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            Console.WriteLine("This migration does nothing, should be created from dump");
            return;
            migrationBuilder.DropTable(
                name: "aka_name");

            migrationBuilder.DropTable(
                name: "aka_title");

            migrationBuilder.DropTable(
                name: "cast_info");

            migrationBuilder.DropTable(
                name: "complete_cast");

            migrationBuilder.DropTable(
                name: "link_type");

            migrationBuilder.DropTable(
                name: "movie_companies");

            migrationBuilder.DropTable(
                name: "movie_info");

            migrationBuilder.DropTable(
                name: "movie_info_idx");

            migrationBuilder.DropTable(
                name: "movie_keyword");

            migrationBuilder.DropTable(
                name: "movie_link");

            migrationBuilder.DropTable(
                name: "person_info");

            migrationBuilder.DropTable(
                name: "char_name");

            migrationBuilder.DropTable(
                name: "role_type");

            migrationBuilder.DropTable(
                name: "comp_cast_type");

            migrationBuilder.DropTable(
                name: "company_name");

            migrationBuilder.DropTable(
                name: "company_type");

            migrationBuilder.DropTable(
                name: "keyword");

            migrationBuilder.DropTable(
                name: "title");

            migrationBuilder.DropTable(
                name: "info_type");

            migrationBuilder.DropTable(
                name: "name");

            migrationBuilder.DropTable(
                name: "kind_type");
        }
    }
}
