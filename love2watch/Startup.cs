﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;

using love2watch.Models;
using Microsoft.Extensions.Logging;

namespace love2watch
{
    public class Startup
    {
        public static bool WasCalledFromMain = false;

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<lovewatchContext>(options => 
                    options.UseNpgsql(Configuration.GetConnectionString("imdb_db")));


            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("love2watch", new Swashbuckle.AspNetCore.Swagger.Info {
                    Title = "l2w API",
                    Version = "v1"
                });
                var basePath = AppContext.BaseDirectory;
                var xmlPath = System.IO.Path.Combine(basePath, "love2watch.xml");
                s.IncludeXmlComments(xmlPath);
            });


			
            services.AddIdentity<ApplicationUser, IdentityRole>()
            .AddEntityFrameworkStores<lovewatchContext>()
            .AddDefaultTokenProviders();

			services.AddMvc().AddJsonOptions(
				options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
			);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                if (Configuration.GetValue<bool>("LogSQL", false))
                {
                    loggerFactory.AddProvider(new MyFilteredLoggerProvider());
                }
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint(
                url: "/swagger/love2watch/swagger.json", description: "Swagger Love2Watch"
            ));

			app.UseAuthentication();
            app.UseMvc();
        }
    }
    /// <summary>
    /// Custom logger factory, creates a logger for each category
    /// </summary>
    public class MyFilteredLoggerProvider : ILoggerProvider
    {
        /// <summary>
        /// Use STDOUT logger for SQL execution only
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns>Null or STDOUT logger</returns>
        public ILogger CreateLogger(string categoryName)
        {
            // NOTE: This sample uses EF Core 1.1. If using EF Core 1.0, then use 
            //       Microsoft.EntityFrameworkCore.Storage.Internal.RelationalCommandBuilderFactory
            //       rather than IRelationalCommandBuilderFactory
            if (categoryName == "Microsoft.EntityFrameworkCore.Database.Command")
            {
                return new MyLogger();
            }

            return new NullLogger();
        }

        /// <summary>
        /// Required by <see cref="ILoggerProvider"/>. I don't know why
        /// </summary>
        public void Dispose()
        { }

    private class MyLogger : ILogger
        {
            public bool IsEnabled(LogLevel logLevel)
            {
                return true;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                Console.WriteLine(formatter(state, exception));
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return null;
            }
        }

        private class NullLogger : ILogger
        {
            public bool IsEnabled(LogLevel logLevel)
            {
                return false;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            { }

            public IDisposable BeginScope<TState>(TState state)
            {
                return null;
            }
        }
    }
    ///<summary>
    ///Extension method to see in context if it's regular run or migration
    ///</summary>
    public static class DbOptExtensions
    {
        ///<summary>
        ///not Startup.WasCalledFromMain
        ///</summary>
        public static bool IsDesignTime(this lovewatchContext dbOpt)
        {
            return !Startup.WasCalledFromMain;
        }
    }
}
